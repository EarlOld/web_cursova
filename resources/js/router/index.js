import Vue from 'vue';
import Router from 'vue-router';

/**
 * Layzloading will create many files and slow on compiling, so best not to use lazyloading on devlopment.
 * The syntax is lazyloading, but we convert to proper require() with babel-plugin-syntax-dynamic-import
 * @see https://doc.laravue.dev/guide/advanced/lazy-loading.html
 */

Vue.use(Router);

/* Layout */
import Layout from '@/layout';

/* Router for modules */
import errorrsRoutes from './modules/error';

/**
 * Sub-menu only appear when children.length>=1
 * @see https://doc.laravue.dev/guide/essentials/router-and-nav.html
 **/

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    roles: ['admin', 'editor']   will control the page roles (you can set multiple roles)
    title: 'title'               the name show in sub-menu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    noCache: true                if true, the page will no be cached(default is false)
    breadcrumb: false            if false, the item will hidden in breadcrumb (default is true)
    affix: true                  if true, the tag will affix in the tags-view
  }
**/

export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index'),
      },
    ],
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true,
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/AuthRedirect'),
    hidden: true,
  },
  {
    path: '/404',
    redirect: { name: 'Page404' },
    component: () => import('@/views/ErrorPage/404'),
    hidden: true,
  },
  {
    path: '/401',
    component: () => import('@/views/ErrorPage/401'),
    hidden: true,
  },
  {
    path: '/',
    component: () => import('@/views/front-page/index'),
    hidden: true,
  },
  {
    path: '',
    component: Layout,
    redirect: 'noredirect',
    name: 'My Settings',
    children: [
      {
        path: '/my-settings',
        component: () => import('@/views/my-settings/index'),
        name: 'MySettings',
        meta: { title: 'My Settings', icon: 'user' },
      },
    ],
  },
];

export const asyncRoutes = [
  errorrsRoutes,
  {
    path: '/projects',
    component: Layout,
    redirect: 'noredirect',
    name: 'Projects',
    children: [
      {
        path: 'create',
        component: () => import('@/views/projects/Create'),
        name: 'CreateProject',
        meta: { title: 'createProject', icon: 'edit' },
        hidden: true,
      },
      {
        path: 'edit/:id(\\d+)',
        component: () => import('@/views/projects/Edit'),
        name: 'EditProject',
        meta: { title: 'editProject', noCache: true },
        hidden: true,
      },
      {
        path: '',
        component: () => import('@/views/projects/List'),
        name: 'ProjectList',
        meta: { title: 'Progects', icon: 'list' },
      },
    ],
  },
  {
    path: '/skills',
    component: Layout,
    redirect: 'noredirect',
    name: 'Skills',
    children: [
      {
        path: '',
        component: () => import('@/views/skills/index'),
        name: 'Skills',
        meta: { title: 'Skills', icon: 'edit' },
      },
    ],
  },
  {
    path: '/small-projects',
    component: Layout,
    redirect: 'noredirect',
    name: 'Small Projects',
    children: [
      {
        path: '',
        component: () => import('@/views/small-projects/index'),
        name: 'Small Projects',
        meta: { title: 'Small Projects', icon: 'list' },
      },
    ],
  },
  {
    path: '/edications',
    component: Layout,
    redirect: 'noredirect',
    name: 'Educations',
    children: [
      {
        path: '',
        component: () => import('@/views/educations/index'),
        name: 'Educations',
        meta: { title: 'Educations', icon: 'list' },
      },
    ],
  },
  {
    path: '/languages',
    component: Layout,
    redirect: 'noredirect',
    name: 'Languages',
    children: [
      {
        path: '',
        component: () => import('@/views/languages/index'),
        name: 'Languages',
        meta: { title: 'Languages', icon: 'language' },
      },
    ],
  },
  {
    path: '/musics',
    component: Layout,
    redirect: 'noredirect',
    name: 'Musics',
    children: [
      {
        path: '',
        component: () => import('@/views/musics/index'),
        name: 'Musics',
        meta: { title: 'Musics', icon: 'list' },
      },
    ],
  },
  {
    path: '/jobs',
    component: Layout,
    redirect: 'noredirect',
    name: 'Jobs',
    children: [
      {
        path: '',
        component: () => import('@/views/jobs/index'),
        name: 'Jobs',
        meta: { title: 'Jobs', icon: 'list' },
      },
    ],
  },
  { path: '*', redirect: '/404', hidden: true },
];

const createRouter = () => new Router({
  mode: 'history',
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes,
});

const router = createRouter();

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}

export default router;
