import request from '@/utils/request';

export function fetchList() {
  return request({
    url: '/skills',
    method: 'get',
  });
}

export function fetchSkill(id) {
  return request({
    url: '/skills/' + id,
    method: 'get',
  });
}

export default fetchList;

export function deleteSkill(id) {
  return request({
    url: '/skills/' + id,
    method: 'delete',
  });
}

export function createSkill(data) {
  return request({
    url: '/skills',
    method: 'post',
    data,
  });
}

export function updateSkill(data) {
  return request({
    url: `/skills/${data.id}`,
    method: 'put',
    data,
  });
}
