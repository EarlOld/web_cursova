import request from '@/utils/request';

export function fetchList(query) {
  return request({
    url: '/musics',
    method: 'get',
    params: query,
  });
}

export default fetchList;

export function fetchMusic(id) {
  return request({
    url: '/musics/' + id,
    method: 'get',
  });
}

export function deleteMusic(id) {
  return request({
    url: '/musics/' + id,
    method: 'delete',
  });
}

export function createMusic(data) {
  return request({
    url: '/musics',
    method: 'post',
    data,
  });
}

export function updateMusic(data) {
  return request({
    url: `/musics/${data.id}`,
    method: 'put',
    data,
  });
}
