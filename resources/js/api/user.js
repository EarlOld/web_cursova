import request from '@/utils/request';
import Resource from '@/api/resource';

class UserResource extends Resource {
  constructor() {
    super('users');
  }

  permissions(id) {
    return request({
      url: '/' + this.uri + '/' + id + '/permissions',
      method: 'get',
    });
  }

  updatePermission(id, permissions) {
    return request({
      url: '/' + this.uri + '/' + id + '/permissions',
      method: 'put',
      data: permissions,
    });
  }
}

export function updateUser(user) {
  return request({
    url: '/users/2',
    method: 'put',
    data: user,
  });
}

export function fetchUser(id) {
  return request({
    url: '/users/2',
    method: 'get',
  });
}

export { UserResource as default };
