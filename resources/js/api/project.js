import request from '@/utils/request';

export function fetchList(query) {
  return request({
    url: '/projects',
    method: 'get',
    params: query,
  });
}

export default fetchList;

export function fetchProject(id) {
  return request({
    url: '/projects/' + id,
    method: 'get',
  });
}

export function deleteProject(id) {
  return request({
    url: '/projects/' + id,
    method: 'delete',
  });
}

export function createProject(data) {
  return request({
    url: '/projects',
    method: 'post',
    data,
  });
}

export function updateProject(data) {
  return request({
    url: `/projects/${data.id}`,
    method: 'put',
    data,
  });
}
