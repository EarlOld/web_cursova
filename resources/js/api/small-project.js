import request from '@/utils/request';

export function fetchList(query) {
  return request({
    url: '/small-projects',
    method: 'get',
    params: query,
  });
}

export default fetchList;

export function fetchProject(id) {
  return request({
    url: '/small-projects/' + id,
    method: 'get',
  });
}

export function deleteProject(id) {
  return request({
    url: '/small-projects/' + id,
    method: 'delete',
  });
}

export function createProject(data) {
  return request({
    url: '/small-projects',
    method: 'post',
    data,
  });
}

export function updateProject(data) {
  return request({
    url: `/small-projects/${data.id}`,
    method: 'put',
    data,
  });
}
