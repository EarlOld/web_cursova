import request from '@/utils/request';

export function fetchList(query) {
  return request({
    url: '/jobs',
    method: 'get',
    params: query,
  });
}

export default fetchList;

export function fetchJob(id) {
  return request({
    url: '/jobs/' + id,
    method: 'get',
  });
}

export function deleteJob(id) {
  return request({
    url: '/jobs/' + id,
    method: 'delete',
  });
}

export function createJob(data) {
  return request({
    url: '/jobs',
    method: 'post',
    data,
  });
}

export function updateJob(data) {
  return request({
    url: `/jobs/${data.id}`,
    method: 'put',
    data,
  });
}
