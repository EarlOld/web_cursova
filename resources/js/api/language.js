import request from '@/utils/request';

export function fetchList(query) {
  return request({
    url: '/languages',
    method: 'get',
    params: query,
  });
}

export default fetchList;

export function fetchLanguage(id) {
  return request({
    url: '/languages/' + id,
    method: 'get',
  });
}

export function deleteLanguage(id) {
  return request({
    url: '/languages/' + id,
    method: 'delete',
  });
}

export function createLanguage(data) {
  return request({
    url: '/languages',
    method: 'post',
    data,
  });
}

export function updateLanguage(data) {
  return request({
    url: `/languages/${data.id}`,
    method: 'put',
    data,
  });
}
