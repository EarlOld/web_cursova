import request from '@/utils/request';

export function fetchList(query) {
  return request({
    url: '/educations',
    method: 'get',
    params: query,
  });
}

export default fetchList;

export function fetchEducation(id) {
  return request({
    url: '/educations/' + id,
    method: 'get',
  });
}

export function deleteEducation(id) {
  return request({
    url: '/educations/' + id,
    method: 'delete',
  });
}

export function createEducation(data) {
  return request({
    url: '/educations',
    method: 'post',
    data,
  });
}

export function updateEducation(data) {
  return request({
    url: `/educations/${data.id}`,
    method: 'put',
    data,
  });
}
