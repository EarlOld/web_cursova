<?php

use App\Laravue\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@laravue.dev',
            'password' => Hash::make('laravue'),
            'description' => 'I am a front-end developer. I have worked with different projects full time at the office, but remote work is preferable to me. At present, there are 6 people who work with me and we are creative, successful team. I worked with scrum methodology and I liked this. My stack is React.js, Vue.js, Node.js, Angular, webpack and other technologies. I have experience with Backbone and MVC architecture. As my latest project is over I am searching for a new opportunity to develop professional skills.',
            'github' => 'https://github.com/EarlOld',
            'location' => 'Zhytomyr, Ukraine',
            'linkedin' => 'https://www.linkedin.com/in/earlold24'
        ]);
        

        $adminRole = Role::findByName(\App\Laravue\Acl::ROLE_ADMIN);
        $admin->syncRoles($adminRole);
        
        $this->call(UsersTableSeeder::class);
    }
}
