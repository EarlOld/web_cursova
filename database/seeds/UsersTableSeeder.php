<?php

use Illuminate\Database\Seeder;
use App\Laravue\Acl;
use App\Laravue\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userList = [
            "Sapozhnyk Dmytro",
        ];

        foreach ($userList as $fullName) {
            $roleName = Acl::ROLE_MANAGER;
            $user = \App\Laravue\Models\User::create([
                'name' => $fullName,
                'email' => 'earlold24@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('admin'),
                'description' => 'I am a front-end developer. I have worked with different projects full time at the office, but remote work is preferable to me. At present, there are 6 people who work with me and we are creative, successful team. I worked with scrum methodology and I liked this. My stack is React.js, Vue.js, Node.js, Angular, webpack and other technologies. I have experience with Backbone and MVC architecture. As my latest project is over I am searching for a new opportunity to develop professional skills.',
                'github' => 'https://github.com/EarlOld',
                'location' => 'Zhytomyr, Ukraine',
                'linkedin' => 'https://www.linkedin.com/in/earlold24'
            ]);

            $role = Role::findByName($roleName);
            $user->syncRoles($role);
        }
    }
}
