<?php

namespace App\Http\Controllers;

use App\Music;
use Illuminate\Http\Request;
use App\Http\Resources\MusicResource;
use App\Http\Resources\MusicsResource;

class MusicController extends Controller
{
    public function index()
    {
        return new MusicsResource(Music::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $music = new Music();
        $music->title = $request->title;
        $music->link = $request->link;

        $music->save();
        return new MusicResource($music);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Music  $music
     * @return \Illuminate\Http\Response
     */
    public function show(Music $music)
    {
        MusicResource::withoutWrapping();
        return new MusicResource($music);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Music  $music
     * @return \Illuminate\Http\Response
     */
    public function edit(Music $music)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Music  $music
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Music $music)
    {
        $music->title = $request->title;
        $music->link = $request->link;

        $music->save();
        return new MusicResource($music);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Music  $music
     * @return \Illuminate\Http\Response
     */
    public function destroy(Music $music)
    {
        $music->delete();

        return new MusicResource($music);
    }
}
