<?php

namespace App\Http\Controllers;

use App\SmallProject;
use Illuminate\Http\Request;
use App\Http\Resources\SmallProjectResource;
use App\Http\Resources\SmallProjectsResource;

class SmallProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new SmallProjectsResource(SmallProject::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $smallProject = new SmallProject();
        $smallProject->title = $request->title;
        $smallProject->link = $request->link;

        $smallProject->save();
        return new SmallProjectResource($smallProject);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SmallProject  $smallProject
     * @return \Illuminate\Http\Response
     */
    public function show(SmallProject $smallProject)
    {
        SmallProjectResource::withoutWrapping();
        return new SmallProjectResource($smallProject);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SmallProject  $smallProject
     * @return \Illuminate\Http\Response
     */
    public function edit(SmallProject $smallProject)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SmallProject  $smallProject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SmallProject $smallProject)
    {
        $smallProject->title = $request->title;
        $smallProject->link = $request->link;

        $smallProject->save();
        return new SmallProjectResource($smallProject);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SmallProject  $smallProject
     * @return \Illuminate\Http\Response
     */
    public function destroy(SmallProject $smallProject)
    {
        $smallProject->delete();

        return new SmallProjectResource($smallProject);
    }
}
