<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function upload(Request $request) {
        return $request->file('file')->store('uploads', 'public');
    }
}
